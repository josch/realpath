This is an implementation of `realpath -m` (also known as `realpath
--canonicalize-missing`) for systems without coreutils. The differences to
`realpath -m` are:

 - different return value when a symlink loop is detected
 - also works if $PWD doesn't exist

The implementation is not POSIX compatible. Namely, POSIX does not specify the
`readlink` utility used by this script. Instead, the script takes the pragmatic
approach and just requires:

 - a POSIX compliant shell (does not require bash), busybox sh will work
 - dirname and basename
 - sed
 - grep -E
 - readlink (without options)

As a result, the script works well under busybox.

The script is a port of [a bash script](https://github.com/bashup/realpaths)
written by PJ Eby and released in the public domain. Likewise, the author of
this script has waived all copyright on it as well. Differences of this
implementation to the one in bash:

 - much slower due to excessive forking
 - tests are pure shell without cram and without .devkit
 - functions are named realpath_foo instead of realpath.foo because a dot is
   not allowed in POSIX shell

To run the tests:

    $ ./test.sh

To verify that the script works with busybox:

    $ mkdir busybox
    $ busybox --install -s busybox
    $ PATH=$(pwd)/busybox /bin/busybox sh ./test.sh
